package Routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetData возвращает данные
func GetData(c *gin.Context) {
	data := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

	c.JSON(http.StatusOK, data)
}
