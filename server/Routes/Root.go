package Routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Root - корневой маршрут
func Root(c *gin.Context) {
	c.HTML(http.StatusOK, "main.html", nil)
}
