package Models

import "time"

// Сотрудники
type Employee struct {
	ID           int       `json:"id"`           // [уникальное] Идентификатор сотрудника
	Full_name    string    `json:"full_name"`    // Полное имя сотрудника
	Post         string    `json:"post"`         // Занимаемая должность
	Phone_number string    `json:"phone_number"` // Контактный номер телефона сотрудника
	Gender       string    `json:"gender"`       // Пол сотрудника
	Salary       int       `json:"salary"`       // Заработная плата сотрудника
	Hire_date    time.Time `json:"hire_date"`    // Дата найма сотрудника
	DepartmentID int       `json:"departmentID"`
}
