package Models

// Сотрудники
type Manager struct {
	ID               int    `json:"id"`               // Идентификатор сотрудника
	MANAGER_FULLNAME string `json:"manager_fullname"` // Имя руководителя
}
