package Models

// Отделы предприятия
type Department struct {
	ID                 int    `json:"id"`                 // [уникальное] Идентификатор отдела
	DEPARTMENT_NAME    string `json:"department_name"`    // Название отдела
	DESCRIPTION        string `json:"description"`        // Описание отдела
	DEPARTMENT_MANAGER string `json:"department_manager"` // Руководитель отдела

	Departments []Employee `gorm:"foreignKey:DepartmentID"`
}
