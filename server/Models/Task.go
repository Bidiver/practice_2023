package Models

import "time"

// Задачи
type Task struct {
	ID                   int       `json:"id"`                   // Идентификатор задачи
	TASK_NAME            string    `json:"task_name"`            // Наименование задачи
	TASK_DESCRIPTION     string    `json:"task_description"`     // Описание задачи
	TASK_REQUEST_DATE    time.Time `json:"task_request_date"`    // Дата запроса выполнения задачи
	TASK_FINISHING_DATE  time.Time `json:"task_finishing_date"`  // Дата завершения выполнения задачи
	TASK_REQUESTER       string    `json:"task_requester"`       // Сотрудник/отдел, запросивший выполнение задачи
	FINISHING_PERCENTAGE int       `json:"finishing_percentage"` // Процентаж выполнения задачи
	TASK_EXECUTIVE       string    `json:"task_executuve"`       // Исполнитель задачи
}
