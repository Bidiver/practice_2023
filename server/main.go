package main

import (
	"Practice/Config"
	"Practice/Models"
	"Practice/Routes"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var err error

	// Инициализация БД
	conn := "host=localhost user=postgres password=postgres dbname=practice sslmode=disable"
	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	// Миграция БД
	Config.DB.AutoMigrate(
		&Models.Employee{},
		&Models.Department{},
		&Models.Task{},
		&Models.Manager{})

	// Инициализация веб-сервера
	r := gin.Default()
	r.Static("/public", "../client/public")

	r.LoadHTMLFiles("../client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/data", Routes.GetData)

	// Запуск веб-сервера
	r.Run(":7000")
}
