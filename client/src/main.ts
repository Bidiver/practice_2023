console.log("Hello!");

fetch("/data", {
    method: "GET"
}).then(r => {
    if (r.status >= 200 && r.status < 300) {
        return r.json()
    } else {
        throw new Error(r.statusText)
    }
}).then((data: any[]) => {
    console.log(data)
})